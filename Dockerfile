FROM martenseemann/quic-network-simulator-endpoint:latest

RUN apt-get update
RUN apt-get install -y git-core libssl-dev python3-dev python3-pip
RUN pip3 install aiofiles asgiref httpbin starlette wsproto
RUN git clone https://github.com/aiortc/aioquic && cd /aioquic && git checkout c2673a5a64dd74dd8aa056cfc5a325c29cd20f55
WORKDIR /aioquic
RUN pip3 install -e .

COPY run_endpoint.sh .
RUN mkdir /logs
RUN chmod +x run_endpoint.sh

ENTRYPOINT [ "./run_endpoint.sh" ]
